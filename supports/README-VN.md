# Máy ảo miễn phí

## SẢN PHẨM ĐƯỢC TẠO BỞI DBT [![OS - Windows](https://img.shields.io/badge/Windows-blue?logo=Windows&logoColor=white)](https://www.microsoft.com/en-us/windows-server) [![OS - Ubuntu](https://img.shields.io/badge/Ubuntu-red?logo=Ubuntu&logoColor=white)](https://ubuntu.com/) [![OS - MAcOS](https://img.shields.io/badge/MacOS-white?logo=Apple&logoColor=black)](https://www.apple.com/macos)

## Ngôn ngữ

[English](https://gitlab.com/dtofficial/rdpvs/-/blob/main/README.md) [Tiếng Việt](https://gitlab.com/dtofficial/rdpvs/-/blob/main/supports/README-VN.md)

## CẬP NHẬT
```
Cập nhật mới vào ngày 25/6/2021
```

## Cấu hình
```
Windows và Linux
- 2-core vCPU
- 7 GB RAM
- 14 GB SSD (Loại trừ dung lượng hệ thống)
```

```
MacOS 10.15
- 3-core vCPU
- 14 GB RAM
- 14 GB SSD (Loại trừ dung lượng hệ thống)
```

## Cài Đặt:
1. Fork dự án này
2. Đi tới trang dự án của bạn, nhấp vào `Settings` và đi tới `Secrets`, sau đó nhấp vào `New Secret` để thêm những bí mật này bên dưới:

Tên bí mật | Dùng Cho | Giá Trị
----- | ----- | -----
`MAC_REALNAME` | Đối với tên hiển thị của người dùng MacOS | Nhập bất kỳ tên nào bạn muốn
`MAC_USER_PASSWORD` | Đối với mật khẩu quản trị hệ thống MacOS | Nhập bất kỳ mật khẩu nào bạn muốn
`NGROK_AUTH_TOKEN` | Đối với **ngrok** sử dụng đường hầm | Truy cập trang web và sao chép khóa API từ https://dashboard.ngrok.com/auth/your-authtoken
`VNC_PASSWORD` | Đối với mật khẩu đăng nhập của xác thực VNC | Nhập bất kỳ mật khẩu nào bạn muốn
`WINDOWS_USER_PASSWORD` | Đối với mật khẩu đăng nhập RDP của Windows 10 | Nhập bất kỳ mật khẩu nào bạn muốn
`LINUX_USERNAME` | Đối với tên người dùng hệ thống linux | Nhập bất kỳ tên nào bạn muốn
`LINUX_USER_PASSWORD` | Đối với shell linux và mật khẩu gốc | Nhập bất kỳ mật khẩu nào bạn muốn
`LINUX_MACHINE_NAME` | Đối với tên máy tính hệ thống Linux | Nhập bất kỳ tên nào bạn muốn
`CHROME_HEADLESS_CODE` | Để gỡ bỏ máy tính để bàn Linux bằng cách sử dụng điều khiển từ xa của google |  Sao chép mã từ [tại đây](https://remotedesktop.google.com/headless) và đăng nhập bằng tài khoản google của bạn, sau đó sao chép mã bên dưới chỗ trống `Debian Linux`. :warning: Mỗi mã chỉ có thể được sử dụng cho một lần, hãy tạo mã khác khi bạn đã sử dụng mã đó.

## Triển khai và Chạy
<details>
    <summary>MacOS</summary>
<br>
    
1. Chuyển đến Tab `Actions` và chọn một trong các quy trình làm việc của hệ thống.

2. Nhấp vào nút `Run Workflow`

3. Chờ một vài phút.

4. Truy cập https://dashboard.ngrok.com/status/tunnels và kiểm tra xem có một đường trực tuyến đang chạy hay không.

5. Sao chép liên kết (**bỏ tcp://**) và vào VNC Viewer (Tải xuống và cài đặt nó), nhập liên kết để kết nối khu vực bạn đã sao chép từ trang web.

6. Điền vào những thông tin đăng nhập đó, trong tên người dùng `koolisw`và mật khẩu từ `VNC_PASSWORD` bạn đã ghi.

7. Thưởng thức!

</details>

<details>
    <summary>Windows 10</summary>
<br>

1. Đầu tiên, hãy bắt đầu các tác vụ của Hệ thống Windows 10.
2. Thứ hai, hãy truy cập https://dashboard.ngrok.com/status/tunnels và kiểm tra xem có một đường trực tuyến đang chạy hay không.
3. Vào ứng dụng Windows Remote Desktop Connection hoặc phần mềm Microsoft Remote Desktop để kết nối với VPS windows 10.
4. THƯỞNG THỨC!

</details>

<details>
    <summary>Linux</summary>
<br>

1. Đầu tiên, bắt đầu các hành động của Hệ thống Linux.
2. Thứ hai, sao chép liên kết từ bảng điều khiển
<img src="https://raw.githubusercontent.com/RealKoolisw/image/main/VirtualMachine-GHAction/sceenshots/0F804C5F-FE8F-45FA-9720-F91F212597DF.png" >
3. Đi tới MacOS Terminal hoặc Windows CMD Terminal hoặc máy khách ssh khác và nhập lệnh được cung cấp. Nhập mật khẩu ssh của bạn sau đó.
<img src="https://raw.githubusercontent.com/RealKoolisw/image/main/VirtualMachine-GHAction/sceenshots/78FE6C5A-7270-4986-AB8F-57EC4C9B4F44.png" >
4. THƯỞNG THỨC!

</details>

## CẢNH BÁO
```
➡️ Không sử dụng nó để khai thác tiền

➡️ Không chạy mã độc, vi rút và các phần mềm nguy hiểm khác

➡️ Không sử dụng CPU và RAM lên 100%

➡️ Không sử dụng để thực hiện các hành vi vi phạm pháp luật của các quốc gia và vùng lãnh thổ

➡️ Không thay đổi hoặc chỉnh sửa bất cứ điều gì nếu bạn không muốn nó chạy không thành công
```

## Nhắc nhở:
:warning: Không đóng cửa sổ này có tên "**provisioner.exe**", nó sẽ khiến quá trình hệ thống cửa sổ bị dừng và ngắt kết nối khỏi Windows RDP.
<img src="https://raw.githubusercontent.com/RealKoolisw/image/main/VirtualMachine-GHAction/sceenshots/9a56f43b-0734-4186-b619-1588c208eb05.png">

:warning: Không cài đặt các bản cập nhật lớn trên máy ảo macos của bạn, nó sẽ phá vỡ quy trình từ xa của bạn!
<img src="https://raw.githubusercontent.com/RealKoolisw/image/main/VirtualMachine-GHAction/sceenshots/Screenshot%202021-02-23%20at%207.35.57%20AM.png">

## Ghi chú
1. Bạn chỉ có thể chạy mỗi máy ảo chạy trong **6** giờ cho thời gian thực thi.  Nếu Máy ảo đạt đến giới hạn này, nó sẽ bị chấm dứt và xóa mọi thứ.
2. Không được đề xuất cho bạn lưu một số tệp quan trọng và nhạy cảm vào mỗi máy, có nguy cơ tất cả các tệp sẽ bị xóa và không thể hoàn tác.
3. Vì chúng tôi đang sử dụng **ngrok**, bạn chỉ có thể chạy một trong **ba** hệ thống đó do giới hạn gói miễn phí của ngrok, bạn không thể truy cập cả hai hệ thống cùng một lúc trừ khi bạn mua **ngrok** pro hoặc gói kinh doanh.
4. Không được đề xuất đăng nhập bất kỳ tài khoản nào từ bất kỳ trang web nào (giống như **Google hoặc Microsoft**), vì kết nối **VNC** không được mã hóa và dễ dàng bị người khác truy cập.
---
