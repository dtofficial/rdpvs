# Free Virtual Machine

## PRODUCTS CREATED BY DBT [![OS - Windows](https://img.shields.io/badge/Windows-blue?logo=Windows&logoColor=white)](https://www.microsoft.com/en-us/windows-server) [![OS - Ubuntu](https://img.shields.io/badge/Ubuntu-red?logo=Ubuntu&logoColor=white)](https://ubuntu.com/) [![OS - MAcOS](https://img.shields.io/badge/MacOS-white?logo=Apple&logoColor=black)](https://www.apple.com/macos)

## Language

[English](#) [Tiếng Việt](https://gitlab.com/dtofficial/rdpvs/-/blob/main/supports/README-VN.md)

## UPDATE
```
New Update On 6/25/2021
```

## Configuration
```
Windows and Linux
- 2-core vCPU
- 7 GB RAM
- 14 GB SSD Disk (Excluded System used)
```

```
MacOS 10.15
- 3-core vCPU
- 14 GB RAM
- 14 GB SSD Disk (Excluded System used)
```

## Setting up:
1. Fork this project
2. Go to your peoject page, click `Settings` and go to `Secrets`, and then click `New Secret` to add these secrets below:

Secrets Name | Uses | Values
----- | ----- | -----
`MAC_REALNAME` | For MacOS User Display Name | Type any name you want
`MAC_USER_PASSWORD` | For MacOS System Admin Password | Type any password you want
`NGROK_AUTH_TOKEN` | For **ngrok** tunnel uses | Go to website, and copy the API key from https://dashboard.ngrok.com/auth/your-authtoken
`VNC_PASSWORD` | For the login password of VNC remote authentication | Type any password you want
`WINDOWS_USER_PASSWORD` | For Windows 10 RDP login password | Type any password you want
`LINUX_USERNAME` | For linux system username | Type any name you want
`LINUX_USER_PASSWORD` | For linux shell and root password | Type any password you want
`LINUX_MACHINE_NAME` | For Linux System Computer name | Type any name you want
`CHROME_HEADLESS_CODE` | For remoting linux desktop using google remote | Copy Codes from [here](https://remotedesktop.google.com/headless) and login with your google account, and then copy the code below `Debian Linux` blank. :warning: Each code can only be used for once, generate another code when u have used that one.

## Deloy and Run
<details>
    <summary>MacOS</summary>
<br>
    
1. Go to `Actions` Tab and select one of system workflow.

2. Click `Run Workflow` button on the left of `This workflow has a workflow_dispatch event trigger` line.

3. Wait until a few minutes.

4. Go to https://dashboard.ngrok.com/status/tunnels and check if theres a one online tunnel running.

5. Copy the link(**without tcp://**) and go to VNC Viewer(Download and install it), input the link to connect area u copied from the website.

6. Fill in those login info, within username `koolisw`and password from `VNC_PASSWORD` you typed.

7. Enjoy!

</details>

<details>
    <summary>Windows 10</summary>
<br>

1. First, start the actions of Windows 10 System.
2. Second, Go to https://dashboard.ngrok.com/status/tunnels and check if theres a one online tunnel running.
3. Go to Windows Remote Desktop Connection app or Microsoft Remote Desktop software to connect to windows 10 VPS.
4. ENJOY!

</details>

<details>
    <summary>Linux</summary>
<br>

1. First, start the actions of Linux System.
2. Second, Copy the link from the console
<img src="https://raw.githubusercontent.com/RealKoolisw/image/main/VirtualMachine-GHAction/sceenshots/0F804C5F-FE8F-45FA-9720-F91F212597DF.png" >
3. Go to MacOS Terminal or Windows CMD Terminal or else ssh client and enter command provided. Enter your ssh password then.
<img src="https://raw.githubusercontent.com/RealKoolisw/image/main/VirtualMachine-GHAction/sceenshots/78FE6C5A-7270-4986-AB8F-57EC4C9B4F44.png" >
4. ENJOY!

</details>

## WARN
```
➡️ Don't use it to mine coins

➡️ Do not run malicious code, viruses and other dangerous software

➡️ Do not use CPU and RAM to 100%

➡️ Do not use it to commit acts that violate the laws of countries and territories

➡️ Don't change or edit anything if you don't want it run fail
```

## Reminders:
:warning: Dont close this windows which called "**provisioner.exe**", it will cause the windows system process to be stopped and disconnect from Windows RDP.
<img src="https://raw.githubusercontent.com/RealKoolisw/image/main/VirtualMachine-GHAction/sceenshots/9a56f43b-0734-4186-b619-1588c208eb05.png">

:warning: Dont install big sur updates on your macos virtual machine, it will break your remote process!
<img src="https://raw.githubusercontent.com/RealKoolisw/image/main/VirtualMachine-GHAction/sceenshots/Screenshot%202021-02-23%20at%207.35.57%20AM.png">

## Notes
1. You can run each virtual machine only run up for **6** hours for execution time. If Virtual Machine reaches this limit, it will be terminated and clear everything.
2. Not suggested for you saving some of important and sensitive files to each machine, be risked that all files will be deleted and cannot be undo.
3. Since we are using **ngrok**, you can only run one of those **three** system by one due to ngrok free plan limits, you cannot access both system at the same time unless you purchased **ngrok** pro or business plan.
4. Not suggested to login any account from any website (just like **Google or Microsoft**), since **VNC** connection is not encrypted and easily been accessed other people.
---
